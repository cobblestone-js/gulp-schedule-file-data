var through = require("through2");
var dotted = require("dotted");
var merge = require("merge");
var moment = require("moment");

module.exports = function(params)
{
    // Normalize the values.
    params = params || {};
    params.scheduleProperty = params.scheduleProperty || "data.schedule";
    params.rootProperty = params.rootProperty || "data";
    params.date = params.date || new Date(Date.now());

    var date = moment(params.date);

    // Method for filtering out elements that haven't passed.
    function isPassed(value)
    {
        var scheduleDate = value["scheduleDate"];
        return scheduleDate && moment(scheduleDate) <= date;
    }

    // Create and return the pipe.
    var pipe = through.obj(
        function(file, encoding, callback)
        {
            // Retrieve the schedule using dotted (to allow for dotted notation).
            // If we don't have it, then skip the file.
            var schedule = dotted.getNested(file, params.scheduleProperty);

            if (!schedule)
            {
                return callback(null, file);
            }

            // If the schedule isn't an array, then make it one.
            if (!Array.isArray(schedule))
            {
                schedule = [schedule];
            }

            // Filter out elements that haven't passed yet.
            schedule = schedule.filter(isPassed);

            if (schedule.length == 0)
            {
                return callback(null, file);
            }

            // Grab the most recent scheduled event.
            schedule.sort(function(a, b) {
                if (a.scheduleDate < b.scheduleDate)
                    return 1;
                if (a.scheduleDate > b.scheduleDate)
                    return -1;
                return 0;
            });

            // Merge the two values together, with chosen taking priority.
            var overrides = merge({}, schedule[0]);
            var values = dotted.getNested(file, params.rootProperty);
            var newValues = merge(values, overrides);

            dotted.setNested(file, params.rootProperty, newValues);

            // Callback so we can continue.
            callback(null, file);
        });

    return pipe;
}
